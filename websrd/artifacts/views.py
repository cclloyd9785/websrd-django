from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden
from django.core import serializers

from websrd.utils import get_avatar, render

from .models import Artifact

def index(request):

    context = {
        'test': 'test'
    }

    return render(request, 'artifacts/new.html', context)


def view(request, id):
    user = request.user
    artifact = get_object_or_404(Artifact, pk=id)

    context = {
        'artifact': artifact,
    }
    return render(request, 'artifacts/view.html', context)


def all(request):
    user = request.user
    gravatar = get_avatar(user)

    artifacts = Artifact.objects.order_by('-id')[:20]

    context = {
        'artifacts': artifacts
    }
    return render(request, 'artifacts/all.html', context)


def my(request):
    user = request.user

    artifacts = Artifact.objects.filter(owner=user.id).order_by('-id')

    context = {
        'artifacts': artifacts
    }
    return render(request, 'artifacts/my.html', context)


def new(request):
    user = request.user

    if request.method == "POST":

        artifact = Artifact(owner=user.id)
        artifact.Name = request.POST['Name']

        artifact.Flavor = request.POST['Flavor']
        artifact.Aura = request.POST['Aura']
        artifact.CL = request.POST['CL']
        artifact.Slot = request.POST['Slot']
        artifact.Weight = request.POST['Weight']
        #artifact.SpecialPower = request.POST['SpecialPower']
        artifact.SpecialPower = request.POST.get('SpecialPower', "")

        artifact.Type = request.POST['Type']
        #artifact.Rarity = request.POST['Rarity']
        artifact.Rarity = request.POST.get('Rarity', "")
        artifact.Mythic = request.POST.get('Mythic', False)
        artifact.Artifact = request.POST.get('Artifact', False)

        artifact.Alignment = request.POST['Alignment']
        artifact.Senses = request.POST['Senses']
        artifact.Communication = request.POST['Communication']
        artifact.Ego = request.POST['Ego']
        artifact.IntScore = request.POST['IntScore']
        artifact.WisScore = request.POST['WisScore']
        artifact.ChaScore = request.POST['Name']
        artifact.SpecialPurpose = request.POST['SpecialPurpose']
        #artifact.SpecialAbility = request.POST['SpecialAbility']
        artifact.SpellLikeAbilities = request.POST.get('SpellLikeAbilities', "")

        artifact.Description = request.POST['Description']
        artifact.Destruction = request.POST['Destruction']
        artifact.Ramifications = request.POST['Ramifications']
        #artifact.DescriptionShort = request.POST['DescriptionShort']
        artifact.DescriptionShort = request.POST.get('DescriptionShort', "")
        artifact.DestructionShort = request.POST.get('DestructionShort', "")
        artifact.RamificationsShort = request.POST.get('RamificationsShort', "")
        #artifact.DestructionShort = request.POST['DestructionShort']
        #artifact.RamificationsShort = request.POST['RamificationsShort']

        #artifact.Construction = request.POST['Construction']
        artifact.Construction = request.POST.get('Construction', "")
        artifact.ConstructionRequirements = request.POST['ConstructionRequirements']
        artifact.Price = request.POST['Price']
        artifact.Cost = request.POST['Cost']

        #artifact.MythicBenefitShort = request.POST.get('MythicBenefitShort', "")

        artifact.save()

        return HttpResponse(serializers.serialize("json", [artifact,]))

    else:
        content = {}
        return render(request, 'artifacts/new.html', content)


def edit(request, id):
    user = request.user
    artifact = get_object_or_404(Artifact, pk=id)

    if artifact.owner is not user.id:
        return HttpResponseForbidden()

    if request.method == "POST":
        artifact.Name = request.POST['Name']

        artifact.Flavor = request.POST['Flavor']
        artifact.Aura = request.POST['Aura']
        artifact.CL = request.POST['CL']
        artifact.Slot = request.POST['Slot']
        artifact.Weight = request.POST['Weight']
        # artifact.SpecialPower = request.POST['SpecialPower']
        artifact.SpecialPower = request.POST.get('SpecialPower', "")

        artifact.Type = request.POST['Type']
        # artifact.Rarity = request.POST['Rarity']
        artifact.Rarity = request.POST.get('Rarity', "")
        artifact.Mythic = request.POST.get('Mythic', False)
        artifact.Artifact = request.POST.get('Artifact', False)

        artifact.Alignment = request.POST['Alignment']
        artifact.Senses = request.POST['Senses']
        artifact.Communication = request.POST['Communication']
        artifact.Ego = request.POST['Ego']
        artifact.IntScore = request.POST['IntScore']
        artifact.WisScore = request.POST['WisScore']
        artifact.ChaScore = request.POST['Name']
        artifact.SpecialPurpose = request.POST['SpecialPurpose']
        # artifact.SpecialAbility = request.POST['SpecialAbility']
        artifact.SpellLikeAbilities = request.POST.get('SpellLikeAbilities', "")

        artifact.Description = request.POST['Description']
        artifact.Destruction = request.POST['Destruction']
        artifact.Ramifications = request.POST['Ramifications']
        # artifact.DescriptionShort = request.POST['DescriptionShort']
        artifact.DescriptionShort = request.POST.get('DescriptionShort', "")
        artifact.DestructionShort = request.POST.get('DestructionShort', "")
        artifact.RamificationsShort = request.POST.get('RamificationsShort', "")
        # artifact.DestructionShort = request.POST['DestructionShort']
        # artifact.RamificationsShort = request.POST['RamificationsShort']

        # artifact.Construction = request.POST['Construction']
        artifact.Construction = request.POST.get('Construction', "")
        artifact.ConstructionRequirements = request.POST['ConstructionRequirements']
        artifact.Price = request.POST['Price']
        artifact.Cost = request.POST['Cost']

        # artifact.MythicBenefitShort = request.POST.get('MythicBenefitShort', "")

        artifact.save()

        return HttpResponse(serializers.serialize("json", [artifact,]))

    else:
        context = {
            'artifact': artifact
        }
        return render(request, 'artifacts/edit.html', context)





