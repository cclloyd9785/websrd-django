from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden
from django.core import serializers
from websrd.utils import render

from .models import Feat

def index(request):





    context = {
        'test': 'test'
    }

    return render(request, 'feats/new.html', context)


def view(request, id):

    feat = get_object_or_404(Feat, pk=id)

    context = {
        'feat': feat
    }
    return render(request, 'feats/view.html', context)

def all(request):

    feats = Feat.objects.order_by('-id')[:20]

    context = {
        'feats': feats
    }
    return render(request, 'feats/all.html', context)

def my(request):
    user = request.user

    feats = Feat.objects.filter(owner=user.id).order_by('-id')

    context = {
        'feats': feats
    }
    return render(request, 'feats/my.html', context)


def new(request):
    user = request.user

    if request.method == "POST":

        feat = Feat(owner=user.id)
        feat.Name = request.POST['Name']
        feat.Prerequisite = request.POST['Prerequisite']
        feat.Flavor = request.POST['Flavor']
        feat.Benefit = request.POST['Benefit']
        feat.BenefitShort = request.POST['BenefitShort']
        feat.Normal = request.POST['Normal']
        feat.MetaFeat = request.POST.get('MetaFeat', False)
        feat.MetaLevelIncrease = request.POST['MetaLevelIncrease']

        feat.Mythic = request.POST.get('Mythic', False)
        feat.MythicFlavor = request.POST['MythicFlavor']
        feat.MythicPrerequisite = request.POST['MythicPrerequisite']
        feat.MythicBenefit = request.POST['MythicBenefit']
        feat.MythicBenefitShort = request.POST.get('MythicBenefitShort', "")

        feat.save()

        return HttpResponse(serializers.serialize("json", [feat,]))

    else:
        content = {}
        return render(request, 'feats/new.html', content)


def edit(request, id):
    user = request.user
    feat = get_object_or_404(Feat, pk=id)

    if feat.owner is not user.id:
        return HttpResponseForbidden()

    if request.method == "POST":
        feat.Name = request.POST['Name']
        feat.Prerequisite = request.POST['Prerequisite']
        feat.Flavor = request.POST['Flavor']
        feat.Benefit = request.POST['Benefit']
        feat.BenefitShort = request.POST['BenefitShort']
        feat.Normal = request.POST['Normal']
        feat.MetaFeat = request.POST.get('MetaFeat', False)
        feat.MetaLevelIncrease = request.POST['MetaLevelIncrease']

        feat.Mythic = request.POST.get('Mythic', False)
        feat.MythicFlavor = request.POST['MythicFlavor']
        feat.MythicPrerequisite = request.POST['MythicPrerequisite']
        feat.MythicBenefit = request.POST['MythicBenefit']
        feat.MythicBenefitShort = request.POST.get('MythicBenefitShort', "")

        feat.save()

        return HttpResponse(serializers.serialize("json", [feat,]))

    else:
        context = {
            'feat': feat
        }
        return render(request, 'feats/edit.html', context)



