// JavaScript Document
$(document).ready(function() {

	/*
	// Remove materialize from markdown containers
	$('.markdown').each(function() {
		var md = $(this);
		
		$('h6:contains("SRDTITLE")').addClass('websrd-header-1');
		$('h5:contains("SRDTITLE")').addClass('websrd-header-1');
		$('h4:contains("SRDTITLE")').addClass('websrd-header-1');
		$('h3:contains("SRDTITLE")').addClass('websrd-header-1');
		$('h2:contains("SRDTITLE")').addClass('websrd-header-1');
		$('h1:contains("SRDTITLE")').addClass('websrd-header-1');
		$('.websrd-header-1').each(function() {
			console.log('text', $(this).text());
			var text = $(this).text().replace(/SRDTITLE/, ' ');
			$(this).html(text);
		});
	});
	*/
	var websrddown = function () {
		var collapse = {
			type: 'lang',
			regex: /SRDCOLLAPSE (.+)\nSRDCOLLAPSESTART(.+)SRDCOLLAPSEEND/g,
			replace: '<div class="srdtitle">$1</div>', 
			/*filter: function(text, converter) {
				//console.log('test', text);
				text = text.replace(/[%]{3}/g, '<div class="divider palette-dark-6"></div>');
				
				
				//console.log('matches', text.match(/\$%%(.+)/g));
				text = text.replace(/SRDCOLLAPSE (.+)\n/g, '<div class="test1">$1</div>');
				console.log('text', text);
				return text;
			}*/
		};
		var divider = {
			type: 'lang',
			regex: /%%%/g,
			replace: '<div class="divider palette-dark-6"></div>',
		};
		return [divider, collapse];
	}
	showdown.extension('websrddown', websrddown);
	var md = $('#feat-benefit');
	var converter = new showdown.Converter({
		tables: true,
		extensions: [
			'websrddown',
		]
	});
	var text = md.text();
	var html = converter.makeHtml(text);
	md.html(html);
});