$(document).ready(function() {
	

	$('#submit-feat-button').on('click', function() {
		console.log('name', $('[name=Name]').val());
		var data = $('#feat-form').serialize();

		if (($('[name=Name]').val() == undefined) || ($('[name=Name]').val().length < 2)) {
			return;
		}



		var url = '/feats/new/';
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			success: function(data) {
				console.log('success', data);
				window.location = site.uri.public + '/feats/' + data.id;
			},
			error: function(error) {
				$('#error').fadeIn(250).delay(2000).fadeOut(250);
			}
		});
	});
	
	
	
});