
$(document).ready(function() {
	$('#edit-save-button').on('click', function() {

		var data = $('#feat-form').serialize();
        url = $('#feat-form').attr('action');
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			success: function(data) {
				$('#saved').fadeIn(250).delay(2000).fadeOut(250);
			},
			error: function(error) {
				$('#error').fadeIn(250).delay(2000).fadeOut(250);
			}
		});
	});	
});

