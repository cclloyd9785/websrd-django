from django.contrib import admin

from .models import Feat

admin.site.register(Feat)