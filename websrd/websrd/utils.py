import hashlib
from django.shortcuts import render as drender
from websrd.settings import DEBUG

def get_avatar(user):
    m = hashlib.md5()
    m.update(user.email.encode('utf-8'))
    gravatar = m.hexdigest()
    return "https://www.gravatar.com/avatar/%s" % gravatar


def render(request, template, context={}):


    site = {
        'title': 'OpenSRD',
        'uri': {
            'public': 'https://opensrd.cclloyd.com',
        }
    }



    if DEBUG:
        site['uri']['public'] = 'http://127.0.0.1:8000'


    gravatar = None


    if request.user.is_authenticated:
        gravatar = get_avatar(request.user)

    global_context = {
        'gravatar': gravatar,
        'site': site,
    }
    return drender(request, template, {**context, **global_context})