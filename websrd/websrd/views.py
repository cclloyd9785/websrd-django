import json, re
from django.shortcuts import get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseNotFound
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_user
from django.contrib.auth import logout as logout_user
from django.views.decorators.http import require_http_methods

from websrd.utils import render


def home(request):
    return render(request, 'websrd/home.html')


def react(request):
    return render(request, 'websrd/react.html')


@require_http_methods(['POST'])
def login(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        if re.compile('.+@.+\..+').match(username):
            user = authenticate(email=username, password=password)
        else:
            user = authenticate(username=username, password=password)

        if user is not None:
            auth_user(request, user)
            return HttpResponse(True, content_type="application/json")
        else:
            response = {
                'success': False,
                'error': True,
                'message': "The username/email or password was incorrect.",
            }
            return HttpResponseForbidden(json.dumps(response))

    else:
        return HttpResponseNotFound("Method not found")


def logout(request):
    ref = request.META['HTTP_REFERER']
    logout_user(request)

    return redirect(ref)


@require_http_methods(['POST'])
def register(request):
    if request.method == "POST":
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']

        user = User.objects.create_user(username, email, password)
        user.save()

        if user.is_authenticated:
            return HttpResponse(json.dumps(True))
        else:
            return HttpResponse(json.dumps(False))

    return False




