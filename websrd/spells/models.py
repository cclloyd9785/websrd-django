from django.db import models
from django.utils import timezone
import datetime
# Create your models here.


class Spell(models.Model):
    owner = models.IntegerField('owner', default=1)

    Name = models.CharField('Name', max_length=128, default="", blank=False)
    School = models.CharField('School', max_length=48, default="", blank=True)
    Subschool = models.CharField('Subschool', max_length=48, default="", blank=True)
    Level = models.CharField('Level', max_length=255, default="", blank=True)
    Domain = models.CharField('Domain', max_length=255, default="", blank=True)
    ElementalSchool = models.CharField('ElementalSchool', max_length=255, default="", blank=True)
    CastingTime = models.CharField('CastingTime', max_length=128, default="", blank=True)
    Components = models.CharField('Components', max_length=128, default="", blank=True)
    ComponentsShort = models.CharField('ComponentsShort', max_length=32, default="", blank=True)
    Range = models.CharField('Range', max_length=128, default="", blank=True)
    Area = models.CharField('Area', max_length=128, default="", blank=True)
    Target = models.CharField('Target', max_length=128, default="", blank=True)
    Duration = models.CharField('Duration', max_length=128, default="", blank=True)
    Save = models.CharField('Save', max_length=128, default="", blank=True)
    SR = models.CharField('SR', max_length=64, default="", blank=True)
    Description = models.CharField('Description', max_length=48, default="", blank=True)
    DescriptionShort = models.CharField('DescriptionShort', max_length=48, default="", blank=True)

    Mythic = models.BooleanField('Mythic', default=False)
    MythicDescription = models.TextField('MythicDescription', default="", blank=True)
    MythicDescriptionShort = models.CharField('MythicDescriptionShort', max_length=255, default="", blank=True)

    Views = models.IntegerField('Views', default=0)

    created = models.DateTimeField('created_at', default=timezone.now)
    modified = models.DateTimeField('modified_at', default=timezone.now)
    deleted = models.DateTimeField('deleted_at', default=None, null=True, blank=True)

    def __str__(self):
        return 'Spell: %s' % self.Name

