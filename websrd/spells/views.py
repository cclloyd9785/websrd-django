from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden
from django.core import serializers
from websrd.utils import render
from django.contrib.auth.decorators import login_required

from .models import Spell


def index(request):
    spells = Spell.objects.order_by('-id')[:20]
    context = {
        'spells': spells
    }
    return render(request, 'spells/all.html', context)


def view(request, id):

    spell = get_object_or_404(Spell, pk=id)

    context = {
        'spell': spell
    }
    return render(request, 'spells/view.html', context)

def all(request):

    spells = Spell.objects.order_by('-id')[:20]

    context = {
        'spells': spells
    }
    return render(request, 'spells/all.html', context)

@login_required
def my(request):
    user = request.user

    spells = Spell.objects.filter(owner=user.id).order_by('-id')

    context = {
        'spells': spells
    }
    return render(request, 'spells/my.html', context)


def new(request):
    user = request.user

    if request.method == "POST":

        spell = Spell(owner=user.id)

        spell.Name = request.POST['Name']
        spell.School = request.POST['School']
        spell.Subschool = request.POST['Subschool']
        spell.Level = request.POST['Level']
        spell.Domain = request.POST['Domain']
        spell.ElementalSchool = request.POST['ElementalSchool']
        spell.CastingTime = request.POST['CastingTime']
        spell.Components = request.POST['Components']
        spell.ComponentsShort = request.POST['ComponentsShort']
        spell.Range = request.POST['Range']
        spell.Area = request.POST['Area']
        spell.Target = request.POST['Target']
        spell.Duration = request.POST['Duration']
        spell.Save = request.POST['Save']
        spell.SR = request.POST['SR']
        spell.Description = request.POST['Description']
        spell.DescriptionShort = request.POST['DescriptionShort']

        spell.Mythic = request.POST.get('Mythic', False)
        spell.MythicDescription = request.POST['MythicDescription']
        spell. MythicDescriptionShort = request.POST['MythicDescriptionShort']

        spell.save()

        return HttpResponse(serializers.serialize("json", [spell,]))

    else:
        content = {}
        return render(request, 'spells/new.html', content)


def edit(request, id):
    user = request.user
    spell = get_object_or_404(Spell, pk=id)

    if spell.owner is not user.id:
        return HttpResponseForbidden()

    if request.method == "POST":
        spell.Name = request.POST['Name']
        spell.School = request.POST['School']
        spell.Subschool = request.POST['Subschool']
        spell.Level = request.POST['Level']
        spell.Domain = request.POST['Domain']
        spell.ElementalSchool = request.POST['ElementalSchool']
        spell.CastingTime = request.POST['CastingTime']
        spell.Components = request.POST['Components']
        spell.ComponentsShort = request.POST['ComponentsShort']
        spell.Range = request.POST['Range']
        spell.Area = request.POST['Area']
        spell.Target = request.POST['Target']
        spell.Duration = request.POST['Duration']
        spell.Save = request.POST['Save']
        spell.SR = request.POST['SR']
        spell.Description = request.POST['Description']
        spell.DescriptionShort = request.POST['DescriptionShort']

        spell.Mythic = request.POST.get('Mythic', False)
        spell.MythicDescription = request.POST['MythicDescription']
        spell.MythicDescriptionShort = request.POST['MythicDescriptionShort']

        spell.save()

        return HttpResponse(serializers.serialize("json", [spell,]))

    else:
        context = {
            'spell': spell
        }
        return render(request, 'spells/edit.html', context)



