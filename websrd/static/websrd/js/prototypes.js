// JavaScript Document


/*
var Events = {
	MESSAGE: 0,
	ROLL: 1,
	
}

(function($) {
    $.fn.bouncebutton = function(options) {
		//Do stuff
		console.log('test');
		console.log('options', options);
		var w = this.width();
		var h = this.height();
		console.log($(this));
		$(this).animate({
			'background-size': 100,
		}, {
			duration: 2000,
			step: function(now, tween) {
				var pos = now/100;
				//var val = Math.sin(now/50 * Math.PI)*.8;
				var val = Math.pow(Math.E, pos*(-1)) * Math.sin(4 * Math.PI * pos + 2*Math.PI)+1;
				//var pos = pos*2.70535;
				//var val = (-1 * Math.pow(pos-1.25, 4))  +  (2*Math.pow(pos-1.25, 2)) + 0.35;
				console.log('pos', pos);
				console.log('val', val*.4);
				//console.log($(this));
				$(this).css('transform', 'scale('+val+')');
			}
		});
    };
}(jQuery));
*/



var FileUpload = function (options) {
	
	
	
	
	for (var key in options) {
		
		if (key == 'ajax') {
			this.ajax = Object.assign({}, this.ajaxDefault, options[key]);
		}
		
		else if (key == 'file') 
			this.file = options[key];
		
		
		
		
	}
};
FileUpload.prototype.file = null;
FileUpload.prototype.files = null;
FileUpload.prototype.success = null;
FileUpload.prototype.error = null;
FileUpload.prototype.data = null;
FileUpload.prototype.upProgress = null;
FileUpload.prototype.downProgress = null;
FileUpload.prototype.testFunc = function(data) {};
FileUpload.prototype.progress = 0;
FileUpload.prototype.parent = 0;
FileUpload.prototype.queueNum = 0;



FileUpload.prototype.ajaxDefault = {
	type: "POST",
	url: "http://example.com",
	xhr: function () {
		var xhr = $.ajaxSettings.xhr();
		xhr.upload.onprogress = function (e) {
            // For uploads
            if (e.lengthComputable) {
                var progress = Math.round((e.loaded / e.total)*100);
				console.log('upload-item-'+this.queueNum);
				$('.upload-item-'+this.queueNum).css('width', this.progress+'%');
				var elem = $('.upload-completed');
				
				//if (progress / 20 = > 5) {
				elem.html(progress);
					//elem.appendTo($('.upload-completed-list'));
					//elem.fadeIn(100).delay(1000).fadeOut(100);
				//}
				
				console.log('id', this.id);
            }
        };
		return xhr;
	},
	success: function (data) {
		console.log('Success', data);
	},
	error: function (error) {
		console.log('Error', error);
	},
	async: true,
	data: {},
	cache: false,
	contentType: false,
	processData: false,
	timeout: 60000
};



FileUpload.prototype.upload = function() {
    $.ajax(ajax);
};




FileUpload.prototype.getName = function() {
    return this.file.name;
};

FileUpload.prototype.upload = function() {
    return this.file.name;
};






var Upload = function (file) {
    this.file = file;
};

Upload.prototype.getType = function() {
    return this.file.type;
};
Upload.prototype.getSize = function() {
    return this.file.size;
};
Upload.prototype.getName = function() {
    return this.file.name;
};
Upload.prototype.doUpload = function () {
    var that = this;
    var formData = new FormData();

    // add assoc key values, this will be posts values
    formData.append("file", this.file, this.getName());
    formData.append("upload_file", true);

    $.ajax({
        type: "POST",
        url: "script",
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data) {
            // your callback here
        },
        error: function (error) {
            // handle error
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }
    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
};