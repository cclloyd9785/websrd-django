
$(document).ready(function() {

function setNavActiveClasses() {
	var elems = $('#leftcol').find('a');
	var index = null;
	var localwords = null;
	var matches = null;
	var activeClass = $('body').attr('data-navactiveclass');
	$('#'+activeClass).addClass('active');
}
function debounce(fn, delay) {
  var timer = null;
  return function () {
    var context = this, args = arguments;
    clearTimeout(timer);
    timer = setTimeout(function () {
      fn.apply(context, args);
    }, delay);
  };
}



	var mainW = $('#centercol').width();
	var mainM = parseInt($('#centercol').css('margin-left'));
	var mainWW = mainW;
	var desktop = false;
	if (mainW >= 1280)
		desktop = true;





	// Remove materialize from markdown containers
	$('.markdown').each(function() {
		var md = $(this);
		md.find('ul').addClass('browser-default');
		md.find('ol').addClass('browser-default');
		md.find('p').addClass('browser-default');
		md.find('h5').addClass('browser-default');
		md.find('h4').addClass('browser-default');
		md.find('h3').addClass('browser-default');

		//$('h5:contains("SRDTITLE")').addClass('websrd-header-1');
		//console.log('test');
	});



	// Materialize activations
	$(".dropdown-button").dropdown();
	$('.collapsible').collapsible();

	// Sidenav setup for left and right column
	$('#left-button').sideNav({
		menuWidth: 200, // Default is 300
		edge: 'left', // Choose the horizontal origin
		closeOnClick: false,
		draggable: true,
		onOpen: function(e) {
			if (desktop) {
				$('#leftcol').attr('data-open', true);
				$('#centercol').css('width', $('#centercol').width() - e.width());
				$('#centercol').css('margin-left', e.width());
			}
		},
		onClose: function(e) {
			$('#leftcol').attr('data-open', false);
			if (desktop) {
				$('#centercol').css('width', $('#centercol').width() + e.width());
				$('#centercol').css('margin-left', 0);
			}
		},
	});
	$('#right-button').sideNav({
		menuWidth: 300, // Default is 300
		edge: 'right', // Choose the horizontal origin
		closeOnClick: false,
		draggable: true,
		onOpen: function(e) {
			$('#rightcol').attr('data-open', true);
			if (desktop) {
				$('#centercol').css('width', $('#centercol').width() - e.width());
			}
		},
		onClose: function(e) {
			$('#rightcol').attr('data-open', false);
			if (desktop) {
				$('#centercol').css('width', $('#centercol').width() + e.width());
			}
		},
	});


	// Show sidebars on desktop by default (programatically show them instead of having duplicate for mobile)
	if (desktop) {
		$('#left-button').sideNav('show');
		$('#right-button').sideNav('show');

		$(window).on('resize', debounce(function() {
			if ($('#leftcol').attr('data-open'))
				$('#left-button').sideNav('show');
			if ($('#rightcol').attr('data-open'))
				$('#right-button').sideNav('show');
		}, 100));
	}

	// Touch actions for mobile devices
	//  - Swipe left/right anywhere onscreen to pull out menu
	if (!desktop) {
		var hammertime = new Hammer($('#centercol')[0]);
		hammertime.on('swiperight', function(e) {
			$('#left-button').sideNav('show');
		});
		hammertime.on('swipeleft', function(e) {
			$('#right-button').sideNav('show');
		});
	}


	// Set the active sections in the left column
	setNavActiveClasses();




/**
* Returns a random integer between min (inclusive) and max (inclusive)
* Using Math.round() will give you a non-uniform distribution!
*/
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}


	function rollDice(count, step) {
		var total = 0;
		var dice = [];
		for (var i=0; i<count; i++) {
			var val = getRandomInt(1, step);
			total += val;
			dice.push(val);
		}
		return dice;
	}

	function setAndRoll(step) {
		$('#dice-roller-count-input').val(1);
		$('#dice-roller-step-input').val(step);
		$('#roll-dice-button').click();
	}

	$('#d20-button').on('click', function() {
		setAndRoll(20);
	});
	$('#d4-button').on('click', function() {
		setAndRoll(4);
	});
	$('#d6-button').on('click', function() {
		setAndRoll(6);
	});
	$('#d8-button').on('click', function() {
		setAndRoll(8);
	});
	$('#d10-button').on('click', function() {
		setAndRoll(10);
	});
	$('#d12-button').on('click', function() {
		setAndRoll(12);
	});







	$('#roll-dice-button').on('click', function() {
		var count = $('#dice-roller-count-input').val();
		var step = $('#dice-roller-step-input').val();
		var result = rollDice(count, step);

		var hasMax = false;
		var hasMin = false;

		console.log('count', count);
		console.log('step', step);
		console.log('result', result);

		var elem = $('.roll-result.template').clone().hide().removeClass('template');



		if (count > 1) {
			var total = 0;

			for (var i=0; i<result.length; i++) {
				total += result[i];
				var piece = $('.roll-result-piece.template').clone().removeClass('template');
				if (result[i] == step)
					hasMax = true;
				if (result[i] == 1)
					hasMin = true;


				piece.html(result[i]);
				if (i<result.length-1)
					piece.append(" + ");
				elem.append(piece);
			}

			var totalElem = $('.roll-result-piece.template').clone().removeClass('template');
			totalElem.html(total);
			totalElem.append(" = ");


			if (hasMax && hasMin)
				elem.addClass('purple darken-4');
			if (hasMax && !hasMin)
				elem.addClass('green darken-4');
			if (!hasMax && hasMin)
				elem.addClass('red darken-4');


			elem.prepend(totalElem);
			$('#dice-results').prepend(elem);
			elem.fadeIn(500);
		}




		if (count == 1) {
			elem.html(result);
			if (result == step)
				elem.addClass('green darken-4');
			else if (result == 1)
				elem.addClass('red darken-4');
		}
		$('#dice-results').prepend(elem);
		elem.fadeIn(500);
	});





	$('.stateid').each(function(e) {
        var elem = $(this);
        var id = elem.attr('data-stateid');
        var states = Storages.localStorage.get('states');

        if (states == undefined)
            states = {};
        if (states[id] == undefined)
            states[id] = 1;
        Storages.localStorage.set('states', states);
        console.log('states', states);
    });

	$('.stateid').on('toggleState', function(e) {
	    var id = elem.attr('data-stateid');
	    var state = Storages.localStorage.get('states')[id];
    });

	$('[data-stateid=rightcol]').on('toggleState', function(e) {
	    if (state) {
            $('#right-button').sideNav('hide');
        }
	    else {
            $('#right-button').sideNav('show');
        }
    });

    $('.stateid').on('toggleState', function(e) {
        elem = $(this);
        id = elem.attr('data-stateid');
        states = Storages.localStorage.get('states');
        states[id] = !states[id];
        Storages.localStorage.set('states', states);
    });

	
});









